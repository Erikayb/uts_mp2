import 'package:flutter/material.dart';

class BukuItemObject {
  String imageName;
  String bookName;
  String priceName;

  String description;
  Color backgroudColor;

  BukuItemObject( {
    this.imageName,
    this.bookName,
    this.priceName,
    this.description = "",
    this.backgroudColor = Colors.green,
  });
}
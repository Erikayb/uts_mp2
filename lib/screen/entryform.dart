import 'package:flutter/material.dart';
class Entryform extends StatefulWidget {
  Entryform({Key key}) : super(key: key);

  @override
  _EntryformState createState() => _EntryformState();
}

class _EntryformState extends State<Entryform> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: Container(
        margin: EdgeInsets.all(20),
        child: Column(children: [
         Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Nama Barang",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Add Picture URL",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => input.isEmpty
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                            obscureText: true,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Description",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => input.isEmpty
                                ? "Please enter a valid email"
                                : null,
                            onSaved: (input) => print(input),
                            obscureText: true,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: InkWell(
                          onTap: (){},
                          child: Container(
                            margin: EdgeInsets.all(0),
                            width: double.infinity,
                            height: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.lightBlue),
                            child: Center(child: Text("Add", style: TextStyle(color: Colors.white),)),
                          ),
                        ),
                      ),
      ],),)),
    );
  }
}
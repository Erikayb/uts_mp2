import 'package:flutter/material.dart';
import 'package:toko_buku/object/buku_item_object.dart';
import 'package:toko_buku/screen/about.dart';
import 'package:toko_buku/screen/entryform.dart';
import 'package:toko_buku/profile.dart';
import 'login_screen.dart';
import 'package:flutter/cupertino.dart';


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  int selectedPage = 0;
  TabController _tabController;
  PageController _pageController;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isSearching = false;

  List<BukuItemObject> books = [

    BukuItemObject(
        imageName: "assets/images/1.png",
        bookName: "EXTRA-POETRY",
        priceName: "Rp. 120.000",
        description:
            "Creating a POETRY book from scratch, i need the money for marketing, paying ny artist and potentially publishing. (Project By : Ibrahim Dandach).",
        backgroudColor: Colors.black),
    BukuItemObject(
        imageName: "assets/images/2.png",
        bookName: "Looking for Alaska",
        priceName: "Rp. 220.000",
        description:
            "Looking for Alaska adalah novel karangan Jhon Green yang terbit pertama kali pada 3 Maret 2005. Jhon Green dianugerahi Michael L.Printz Award tahun 2006 untuk novel ini.",
        backgroudColor: Colors.red),
    BukuItemObject(
        imageName: "assets/images/3.png",
        bookName: "Twelfth Night",
        priceName: "Rp. 310.000",
        description:
            "Is a romatic comedy by William Shakespeare, believed to have been written around 1601-1602 as a Twelfth Night's entertainment for the close of the Christmas season.",
        backgroudColor: Colors.purple)
  ];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 7, vsync: this);
    _pageController = PageController(initialPage: 0, viewportFraction: 0.8);
  }

  _makeOrder() {
    // Navigator.push(context, MaterialPageRoute(builder: (_) => OrderScreen()));
  }

  _logOut() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (_) => LoginScreen()));
  }

  _searchPressed() {
    isSearching = !isSearching;
    setState(() {});
  }

  Widget _bookselector(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3)).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 500,
            width: Curves.easeInOut.transform(value) * 400,
            child: widget,
          ),
        );
      },
      child: GestureDetector(
        onTap: () {
        },
        child: Stack(
          children: <Widget>[
            Container(
              height: 400,
              margin: EdgeInsets.only(left: 20, right: 20),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [Colors.green, Colors.yellowAccent], stops: [0, 1]),
                  borderRadius: BorderRadius.circular(25)),
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Hero(
                      tag: books[index].imageName,
                      child: Image(
                        fit: BoxFit.cover,
                        image: AssetImage(books[index].imageName),
                      ),
                    ),
                  ),

                  Positioned(
                    bottom: 25,
                    left: 20,
                    child: Column(
                      children: <Widget>[
                        Text(books[index].bookName, 
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.bold)),
                  Positioned(
                    bottom: 25,
                    left: 20,
                    child: Column(
                      children:<Widget>[
                        Text(books[index].priceName,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold)),
                          ],
                         )
                       ,)
                      ],
                    ),
                  ),
                ],
              ),
            ),
            
            Positioned(
              bottom: 10,
              left: 10,
              right: 10,
              child: RawMaterialButton(
                padding: EdgeInsets.all(15),
                child: Icon(
                  Icons.add_shopping_cart,
                  size: 30,
                  color: Colors.white,
                ),
                fillColor: Colors.black,
                shape: CircleBorder(),
                elevation: 2.0,
                onPressed: () => _makeOrder(),
              ),
            )
          ],
        ),
      ),
    );
  }

  _mainWidgetSwitcher(bool isSearching) {
    return !isSearching
        ? Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Hero(
                    tag: "drawer_button",
                    child: Padding(
                      padding: const EdgeInsets.only(left: 25.0),
                      child: IconButton(
                          icon: Icon(Icons.menu, size: 30),
                          onPressed: () =>
                              _scaffoldKey.currentState.openDrawer()),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () => _searchPressed()))
                ],
              ),
              Container(
                padding: const EdgeInsets.all(25.0),
                child: Text(
                  "Best NOVEL Books",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                ),
              ),
              TabBar(
                controller: _tabController,
                indicatorColor: Colors.transparent,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey.withOpacity(0.6),
                labelPadding: EdgeInsets.symmetric(horizontal: 35),
                isScrollable: true,
                tabs: <Widget>[
                  Tab(
                    child: Text(
                      "Romance",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Comedy",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Horror",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Adventure",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Inspiratif",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Mystery",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Tab(
                    child: Text(
                      "Science Fiction",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 450,
                width: double.infinity,
                child: PageView.builder(
                  controller: _pageController,
                  onPageChanged: (int index) {
                    setState(() {
                      this.selectedPage = index;
                    });
                  },
                  itemCount: books.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _bookselector(index);
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("Description:",
                    style: TextStyle(fontWeight: FontWeight.bold)),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(books[selectedPage].description),
              )
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: TextField(
                        onSubmitted: (String text) {
                          if (text.length == 0) {
                            _searchPressed();
                          }
                        },
                        autofocus: true,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: 'Look for Books'),
                      ),
                    ),
                  ),
                  Padding(
                      padding: const EdgeInsets.only(right: 25.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () => _searchPressed())),
                ],
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) =>
                  //             BukuDetailScreen(Buku: books[selectedPage])));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Sugestion 1")),
                ),
              ),
              InkWell(
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Sugestion 2")),
                ),
              ),
              InkWell(
                onTap: () {
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (_) =>
                  //             BukuDetailScreen(buku: stock[selectedPage])));
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 20),
                  child: Container(
                      width: double.infinity, child: Text("Sugestion 3")),
                ),
              ),
            ],
          );
  }

  Widget bookselector(int index) => _bookselector(index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Container(
          width: double.infinity,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      child: _mainWidgetSwitcher(isSearching))
                ],
              ),
            ),
          ),
        ), // This
        drawer: Drawer(
          elevation: 10,
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.yellow, Colors.green],
                    stops: [0, 1],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter)),
            child: SafeArea(
                child: Stack(
              children: <Widget>[
                Hero(
                  tag: "drawer_button",
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25.0),
                    child: IconButton(
                        icon: Icon(Icons.arrow_back, size: 30),
                        onPressed: () => Navigator.of(context).pop()),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                        child: Container(
                      width: double.infinity,
                      height: 150,
                      child: Stack(
                        children: <Widget>[
                          // Positioned(
                          //   bottom: 50,
                          //   child: Container(
                          //     color: Colors.black,
                          //     width: 400,
                          //     height: 150,
                          //   ),
                          // ),
                          Center(
                            child: Container(
                              height: 150,
                              width: 150,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                    fit: BoxFit.cover,
                                    image:AssetImage(
                                      'assets/images/login.png',
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                    )),
                    // Center(
                    //   child: Column(
                    //       crossAxisAlignment: CrossAxisAlignment.start,
                    //       children: <Widget>[
                    //         Text("Erika Yuni Bastian"),
                    //         Text("18282006")
                    //       ]),
                    // ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: () {
                          _switchToCalendar();
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Center(
                                child: Text("About",
                                    style: TextStyle(color: Colors.grey))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                      Entryform()));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25))),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Center(
                                child: Text("Input Data",
                                    style: TextStyle(color: Colors.grey))),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: InkWell(
                        onTap: (){
                          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                      Profile()));
                        },
                      child: Container(
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Center(
                              child: Text("Profile",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    ),
                    ),
                    InkWell(
                      onTap: () => _logOut(),
                      child: Container(
                        decoration: BoxDecoration(color: Colors.transparent),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Center(
                              child: Text("Logout",
                                  style: TextStyle(color: Colors.white))),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )),
          ),
        ) // trailing comma makes auto-formatting nicer for build methods.
        );
  }
  _switchToCalendar(){
    // Navigator.of(context).push(
    //   FadeRoute(
    //     page: CalendarScreen(),
    //   ),
    // );
     Navigator.push(context, MaterialPageRoute(builder: (_)=>Aboutpage()));
     Navigator.push(context, MaterialPageRoute(builder: (_)=>Profile()));
  }
}

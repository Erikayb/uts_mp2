import 'dart:ui';

import 'package:flutter/material.dart';

class Aboutpage extends StatefulWidget {
  Aboutpage({Key key}) : super(key: key);

  @override
  _AboutpageState createState() => _AboutpageState();
}

class _AboutpageState extends State<Aboutpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Toko Buku"),),
      drawer: Drawer(),
      body: SafeArea(child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top:30, left:20, right:20),
       child: Column(
         children: [
           Container(
             child: Image.asset("assets/images/login.png")
           ),
           SizedBox(
             height: 20,
           ),
           Container(
             child: Text("Aplikasi Toko Buku ini adalah salah satu tempat belanja buku online yang dibangun untuk menjual produk-produk berupa buku dan sejenisnya. Kenyamanan dan harga adalah pedoman kami dengan respon yang cepat kami akan siap melayani kebutuhan anda. ",style: TextStyle(fontSize: 18, ),textAlign: TextAlign.center,)
           ),
           SizedBox(
             height: 250,
           ),
           Container(
             alignment: Alignment.bottomCenter,
            //  color: Colors.black45,
             padding: EdgeInsets.all(21),
             child: Column(
               children: [
                 _copyright("© Erika Yuni Bastian"),
                 _copyright("18282006"),
                 _copyright("2021")

               ],
             ),
           )
         ],),
    ),)
    );
  }

  Widget _copyright(String data){
    return Text(data, style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),);
  }
}